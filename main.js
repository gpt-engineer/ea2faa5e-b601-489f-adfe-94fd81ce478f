document.getElementById('send-btn').addEventListener('click', function() {
    var input = document.getElementById('chat-input');
    var message = input.value;
    input.value = '';

    // Append the user's message to the chat history
    var chatHistory = document.getElementById('chat-history');
    chatHistory.innerHTML += '<p><strong>You:</strong> ' + message + '</p>';

    // Send the message to the back-end
    fetch('GPTENG:api_endpoint', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            query: message
        })
    })
    .then(response => response.json())
    .then(data => {
        // Append the response to the chat history
        chatHistory.innerHTML += '<p><strong>Database:</strong> ' + data.response + '</p>';
    })
    .catch(error => console.error('Error:', error));
});
